# Hybrid-assembly pipeline

[![Nextflow](https://img.shields.io/badge/nextflow-%E2%89%A520.10.0-brightgreen.svg)](https://www.nextflow.io/)

This hybrid assembly pipeline is built using [Nextflow](https://www.nextflow.io), contains a workflow for *De-novo* assembly using Illumina reads and Oxford Nanopore reads.

![Pipeline](src/workflow.png)

## Requirements

- [Nextflow](https://www.nextflow.io)

## Workflow
The workflow contains:

1. Quality Control of input reads.
2. Error-Correction for Oxford Nanopore reads.
3. Assessment of Oxford Nanopore reads quality after and before of error-correction.
4. Assembly.
5. Assessment of assembly quality.

## Input

**Flag**        | **Description**
--------------- | -------------
-params-file    |  Yaml file with the params input
--help          |  Display help
--version       |  Display pipeline version

## Parameters

**Flag**            | **Description**
---------------     | -------------
--assembler         |  Assembler tool (Flye or Canu)
--prefix-assembly   |  Prefix for assembly files
--prefix-qc         |  Prefix for quality control files
--shortReads        |  Illumina reads
--longReads         |  Oxford Nanopore reads
--reference         |  Reference genome fasta file
--genome-name       |  Name of Genome (Optional)
--genome-size       |  Genome size
--genome-unit       |  Unit of genome size (k, m, g or null)
--threads           |  Maximum of threads
--outdir            |  Output directory
--outreport         |  Output report nextflow directory
--fastp-Q           |  Quality of filter
--fastp-fTrim       |  Front trimming size
--fastp-tTrim       |  Tail trimming size
--q_format          |  Qualimap report format
--lordec-k          |  Size of k-mer for correction
--lordec-s          |

## Usage

`nextflow run main.nf -params-file input.yaml`
