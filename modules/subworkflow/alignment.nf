
include { minimap2; samtools } from "$baseDir/modules/process/alignment_processes"
include { multiqc; qualimap } from "$baseDir/modules/process/reports_processes"

workflow alignment {
    take:
        ont
        reference

    main:
        minimap2( ont, reference )
        samtools( minimap2.out )

    emit:
        samtools.out.bam
}


workflow quality_evaluation_reads {
    take:
        ont_reads
        reference

    main:
        alignment( ont_reads, reference.mix(reference) )
        qualimap( alignment.out )
        multiqc( "qualimap", qualimap.out.collect() )
}