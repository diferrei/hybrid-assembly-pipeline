
include { fastqc; multiqc; qualimap; quast; } from "$baseDir/modules/process/reports_processes"
include { quality_evaluation_reads } from "$baseDir/modules/subworkflow/alignment"

workflow evaluation_reports {
    take:
        assembly_reads
        ont_reads
        reference
    
    main:
        quality_evaluation_reads( ont_reads, reference )
        quast( assembly_reads )
        multiqc( "quast", quast.out.collect() )

}