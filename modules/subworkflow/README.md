# Sub-Workflows
This module contains the subworkflow for each procedure.

- Quality Filter

![Quality_filter](/src/quality_filter.png)

- Error-Correction

- Assembly

- Alignment
