
include { flye; canu; nextDenovo; pilon } from "$baseDir/modules/process/assembly_processes"
include { bwa; samtools } from "$baseDir/modules/process/alignment_processes"
include { runFile } from "$baseDir/modules/functions/functions"

workflow assembly {
    take:
        sreads
        reads

    main:
        assembler = params.assembler.toLowerCase()
        if( assembler == "flye" ) {
            flye( reads ) | set { results }
        }
        else if( assembler == "canu" ) {
            canu( reads ) | set { results } 
        }
        else if( assembler == "nextdenovo" ) {
            nextDenovo( reads, runFile() ) | set { results } 
        }
        assembly_polish( results.assembly_result, sreads )
    
    emit:
        assembly = results.assembly_result
        polish = assembly_polish.out
}


workflow assembly_polish {
    take:
        reads
        sreads

    main:
        bwa( sreads, reads )
        samtools( bwa.out )
        pilon(reads, samtools.out.bam)

    emit:
        pilon.out.assembly_polish

}
