
include { fastp; seqkit } from "$baseDir/modules/process/quality-filter_processes"
include { separateIllONT } from "$baseDir/modules/functions/functions"
include { fastqc; multiqc } from "$baseDir/modules/process/reports_processes"

workflow quality_filter {
    take:
        ill_fastq
        ont_fastq
        formats
        
    main:
        seqs = Channel.empty()
        fq2fa = Channel.empty()
        reports = Channel.empty()
        if( formats[0][1] == "fastq" ){
            fastp( ill_fastq )
            if( formats[1][1] == "fastq" ){
                fastp.out.filter_fastq
                    .mix( ont_fastq )
                    .tap { seqs }
                    .mix( ill_fastq )
                    .tap { reads_fq }
            } else {
                fastp.out.filter_fastq
                    .tap { seqs }
                    .mix( ill_fastq )
                    .tap { reads_fq }
            }
        } else if( formats[1][1] == "fastq" ) {
            ont_fastq
                .set { seqs; reads_fq }
        }
        if( formats.any{ x -> x[1] == "fastq" } ){
            seqkit( seqs )
                .set { fq2fa }
            fastqc( reads_fq )
            multiqc( "fastqc", fastqc.out.collect() )
        }
    
    emit:
        ill = separateIllONT( formats[0], fq2fa, ill_fastq )
        ont = separateIllONT( formats[1], fq2fa, ont_fastq )
        
        
}