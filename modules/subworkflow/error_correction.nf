
include { lordec; preprocessing } from "$baseDir/modules/process/error_correction_processes"


workflow error_correction {
    take:
        ill_fasta
        ont_fasta

    main:
        preprocessing( ill_fasta, ont_fasta )
        lordec( ill_fasta, preprocessing.out )


    emit:
        preprocessing = preprocessing.out  
        lordec = lordec.out.correct_reads
}
