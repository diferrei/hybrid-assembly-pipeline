
outdir_assembly = "${params.outdir}/assembly"


process flye {
    label 'flye'
    publishDir "${outdir_assembly}", mode: 'copy'

    input:
        tuple val(name), path(fasta_correction)
    
    output:
        tuple val(prefix), path("flye/${prefix}.fasta"), emit: assembly_result
        path "flye"

    script:
    prefix = "${params.prefix.assembly}_assembly"
    genome_size = "${params.genome.size}${params.genome.unit}"
    """
    flye --nano-raw ${fasta_correction} \
        -g ${genome_size} \
        --out-dir ${params.assembler} \
        --threads ${params.threads}
    mv flye/assembly.fasta flye/${prefix}.fasta
    """
}


process canu {
    label 'canu'
    publishDir "${outdir_assembly}", mode: 'copy'

    input:
        tuple val(name), path(fasta_correction)
    
    output:
        tuple val(prefix), path("canu/${prefix}.contigs.fasta"), emit: assembly_result
        path "*"

    script:
    prefix = "${params.prefix.assembly}_assembly"
    genome_size = "${params.genome.size}${params.genome.unit}"
    """
    canu -nanopore-raw ${fasta_correction} \
        -p ${prefix} \
        -d ${params.assembler} \
        genomeSize=${genome_size} \
        maxThreads=${params.threads}
    """
}

process nextDenovo {
    label 'nextDenovo'
    publishDir "${outdir_assembly}/${params.assembler}", mode: 'copy'
    
    input:
        tuple val(name), path(fasta_correction)
        path run_file

    output:
        tuple val(prefix), path("${prefix}.fasta"), emit: assembly_result
        path "*"
    
    script:
    prefix = "${params.prefix.assembly}_assembly"
    """
    ls ${fasta_correction} > input.fofn
    /usr/local/NextDenovo/./nextDenovo ${run_file}
    mv 03.ctg_graph/nd.asm.fasta ${prefix}.fasta
    """
}

process pilon {
    label 'pilon'
    publishDir "${outdir_assembly}/${params.assembler}", mode: 'copy'

    input:
        tuple val(name_assembly), path(assembly)
        tuple val(name_bam), path(bam)

    output:
        tuple val(prefix), path("pilon/${prefix}.fasta"), emit: assembly_polish
        path "*"

    script:
    prefix = "${params.prefix.assembly}_polish"
    """
    samtools index ${bam}
    pilon --genome ${assembly} \
        --bam ${bam} \
        --output ${prefix} \
        --outdir pilon \
        --changes
    """
}