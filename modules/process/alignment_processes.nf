
outdir_alignment = "${params.outdir}/alignment"

process bwa {
    label 'bwa'
    tag "${name}"
    publishDir "${outdir_alignment}/minimap2", mode: 'copy'

    input:
        tuple val(name), path(read)
        tuple val(rname), path(reference)

    output:
        tuple val(name), path("${name}.sam")

    script:
    """
    bwa index ${reference}
    bwa mem ${reference} ${read} > ${name}.sam
    """

}


process minimap2 {
    label 'minimap2'
    tag "${name}"
    publishDir "${outdir_alignment}/minimap2", mode: 'copy'

    input:
        tuple val(name), path(read)
        path reference

    output:
        tuple val(name), path("${name}.sam")

    script:
    """
    minimap2 ${read} -a ${reference} -o ${name}.sam
    """
}


process samtools {
    label 'samtools'
    tag "${name}"
    publishDir "${outdir_alignment}/samtools", mode: 'copy'

    input:
        tuple val(name), path(sam)
    
    output:
        tuple val(name), path("${name}.sorted.bam"), emit: bam
        path "*"
    
    script:
    """
    samtools view -S -b ${sam} > ${name}.bam
    samtools sort ${name}.bam -o ${name}.sorted.bam
    """
}


