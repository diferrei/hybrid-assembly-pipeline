
outdir_error_correction = "${params.outdir}/error_correction"

process preprocessing {
    label 'julia'
    publishDir "${outdir_error_correction}/preprocessing", mode: 'copy'

    input:
        tuple val(ill_name), path(ill_fasta)
        tuple val(ont_name), path(ont_fasta) 

    output:
        tuple val("${ont_name}_improved"), path("${ont_name}_improved.fasta")

    script:
    """
    julia --project=${baseDir}/src/Preprocessing/@. \
        --threads ${params.threads} \
        ${baseDir}/src/Preprocessing/preprocess.jl \
        --ont ${ont_fasta} \
        --ill ${ill_fasta} \
        --out ${ont_name}_improved.fasta \
        --t 1
    """
}


process lordec {
    label 'lordec'
    publishDir "${outdir_error_correction}/lordec", mode: 'copy'
    
    input:
        tuple val(ill_name), path(ill_fasta)
        tuple val(ont_name), path(ont_fasta)
    
    output:
        tuple val(new_name), path("${new_name}.fasta"), emit: correct_reads
        path "*"

    script:
    new_name = "${ont_name}_correction"
    """
    lordec-correct -2 ${ill_fasta} \
        -i ${ont_fasta} \
        -k ${params.lordec.k} \
        -s ${params.lordec.s} \
        -o ${new_name}.fasta \
        -T ${params.threads}
    """
}
