# Processes
This modules contains the processses for the procedure of each sub-workflows

- Quality Filter:   This module contains the tools for the quality control and quality filter (fastp, fastqc, multiqc, seqkit).
- Error-correction: This module contains the tools for error correction with a pre-processing step (pre-processing, lordec).
- Alignment:    This module contains the tools for the evaluation of reads after and before of pre-processing error-correction (minimap2, samtools, qualimap.
- Assembly: This module contains the tools for the *De-novo assembly* and assembly evaluation (flye, canu, quast).