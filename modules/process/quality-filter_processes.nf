
outdir_quality = "${params.outdir}/quality_control"
outdir_reports = "${params.outdir}/reports"

process fastp {
    label 'fastp'
    tag "${name}"
    publishDir "${outdir_quality}/fastp", mode: 'copy'

    input:
        tuple val(name), path(fastq)

    output:
        tuple val("${subprefix}"), path("${subprefix}.fastq"), emit: filter_fastq
        path "*"

    script:
        if( params.prefix.qc ) { subprefix = "${name}_${params.prefix.qc}" }
        else { subprefix = "${name}_qc" }

        fTrim = ""
        tTrim = ""
        if( params.fastp.fTrim ) { fTrim = "--trim_front1 ${params.fastp.fTrim}" }
        if( params.fastp.tTrim ) { tTrim = "--trim_tail1 ${params.fastp.tTrim}" }
    """
    mkdir report
    fastp --in1 ${fastq} \
        --out1 ${subprefix}.fastq \
        --qualified_quality_phred ${params.fastp.Q} \
        ${fTrim} \
        ${tTrim} \
        --thread ${task.cpus} \
        --html ${outdir_reports}/fastp/${subprefix}.html \
        --json ${outdir_reports}/fastp/${subprefix}.json \
        --report_title Report_${subprefix}
    """
}


process seqkit {
    label 'seqkit'
    tag "${name}"
    publishDir "${params.outdir}/fasta", mode: 'copy'

    input:
        tuple val(name), path(fastq)

    output:
        tuple val(subname), path("${name}.fasta")

    script:
        if( params.prefix.qc ) { subprefix = params.prefix.qc }
        else { subprefix = "qc" }
        subname = name - ~/_${subprefix}/
    """
    seqkit fq2fa ${fastq} \
        --out-file ${name}.fasta \
        --threads ${task.cpus}
    """
}