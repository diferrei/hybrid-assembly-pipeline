
outdir_reports = "${params.outdir}/reports"


process fastqc {
    label 'fastqc'
    tag "${name}"
    publishDir "${outdir_reports}/fastqc/individual_reports", mode: 'copy'

    input:
        tuple val(name), path(fastq)

    output:
        path "${name}*"

    script:
    """
    mkdir ${name}
    fastqc ${fastq} --outdir ${name} \
        --format fastq \
        --threads ${task.cpus}
    """
}

process quast {
    label 'quast'
    publishDir "${outdir_reports}/quast", mode: 'copy'

    input:
        tuple val(name), path(assembly)
    
    output:
        path "*"

    script:
    """
    quast ${assembly} \
        --output-dir ${params.assembler}_${name} \
        --threads ${task.cpus}
    """
}

process qualimap {
    label 'qualimap'
    tag "${name}"
    publishDir "${outdir_reports}/qualimap/individual_reports", mode: 'copy'

    input:
        tuple val(name), path(bam)

    output:
        path "*"

    script:
    """
    qualimap bamqc -bam ${bam} \
        -outdir ${name} \
        -outfile ${name} \
        -outformat ${params.qualimap.format} \
        --java-mem-size=${params.qualimap.memory}G
    """
}


process multiqc {
    label 'multiqc'
    publishDir "${outdir_reports}", mode: 'copy'

    input:
        val tool_name
        path fasta_reports

    output:
        path "*"

    script:
    outdir = "${tool_name}/multiqc"
    """
    multiqc . --outdir ${outdir}
    """
}
