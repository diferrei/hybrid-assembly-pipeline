
def nameCollect( read ) {
    return read.flatMap { it[-1].collect { x -> [x.simpleName, it] } }
}

def recognizerFileFormat( file_format ){

    fasta_format = ["fasta", "fna", "ffn", "faa", "frn", "fa"]
    fastq_format = ["fastq", "fq"]
    file_extension = file_format.tokenize('.')[-1]
    file = file_format.tokenize('/')[-1]
    file_name = file.substring( 0, file.lastIndexOf('.') )

    if( fasta_format.contains(file_extension) ){ return [file_name, "fasta"] } 
    else if ( fastq_format.contains(file_extension) ) { return [file_name, "fastq"] } 
    else { return null }
}

def separateIllONT( format, fq2fa, fasta ){
    if( format[1] == "fastq" ){
        seq = fq2fa.filter { it[0] == format[0] }
    } else { seq = fasta }
    return seq
}

def runFile() {
    general = params.nextDenovo.general
    correct = params.nextDenovo.correct
    assemble = params.nextDenovo.assemble

    general << [parallel_jobs: params.threads, input_fofn:"./input.fofn", workdir: "./"]
    
    run_file = new File('run.cfg')
    run_file.withWriter { line ->
        line << "[General]\n"
        general.forEach { option, value ->
            line << option + " = " + value + "\n"
        }
        line << "\n[correct_option]\n"
        correct.forEach { option, value ->
            line << option + " = " + value + "\n"
        }
        line << "\n[assemble_option]\n"
        assemble.forEach { option, value ->
            line << option + " = " + value + "\n"
        }
    }
    return Channel.fromPath('run.cfg')
}


def helpMessage() {
    log.info"""
    ===========================================================
    Hybrid assembly with Nanopore Correction v${params.version}
    ===========================================================
    Usage:

    nextflow run main.nf -params-file input.yaml

    Principal arguments:
    --assembler         The assembler pipeline to choose, 'Canu' | 'Flye'. Default: 'Flye'
    --shortReads        The short reads
    --longReads         The long reads
    --reference         Genome reference
    --genomeSize        Genome size
    
    Other arguments:
    --outdir            The output directory where the results will be saved
    --lordec-k          The length of the k-mers for LoRDEC. Default: 21
    --lordec-s          The solidity threshold, minimal number of occurrences of a k-mer for LoRDEC. Default: 1

    """.stripIndent()
}