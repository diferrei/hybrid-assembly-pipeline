#!/usr/bin/env nextflow
nextflow.enable.dsl=2

include { quality_filter } from "$baseDir/modules/subworkflow/quality_filter"
include { error_correction } from "$baseDir/modules/subworkflow/error_correction"
include { assembly } from "$baseDir/modules/subworkflow/assembly"
include { evaluation_reports } from "$baseDir/modules/subworkflow/reports"

include { nameCollect; helpMessage; recognizerFileFormat } from "$baseDir/modules/functions/functions"

// Show help message
if (params.help){
    helpMessage()
    exit 0
}


short_reads_dir = "${params.shortReads}"
long_reads_dir = "${params.longReads}"
reference_dir = "${params.reference}"

assembler_list =  ["canu","flye", "nextdenovo"]
assembler = params.assembler.toLowerCase()
format_files = [recognizerFileFormat(short_reads_dir), recognizerFileFormat(long_reads_dir)]


// data preparation
Channel
    .fromPath( short_reads_dir, checkIfExists:true )
    .ifEmpty { exit 1, "Cannot find any reads matching: ${short_reads_dir}" }
    .set { short_reads_ch }

Channel
    .fromPath( long_reads_dir,  checkIfExists:true )
    .ifEmpty { exit 1, "Cannot find any reads matching: ${long_reads_dir}" }
    .set { long_reads_ch }


if(params.reference){
    Channel
        .fromPath( reference_dir, checkIfExists:true )
        .ifEmpty { exit 1, "Cannot find any reads matching: ${long_reads_dir}" }
        .set { reference_ch }
} else {
    reference_ch = long_reads_ch
}

// Validate inputs
if ( params.genome.size == 0 ) {
    log.error "No genome size specified. Necessary for workflow"
    exit 1
}

if ( !assembler_list.contains(assembler) ) {
    log.error "Cannot find any assembler matching: ${params.assembler}.\nPlease choose 'flye' | 'canu'"
    exit 1
}


log.info """\
===========================
 HYBRID ASSEMBLY  PIPELINE
===========================
Genome:             ${params.genome.name}
Reads:
    Short:          ${params.shortReads}
    Long:           ${params.longReads}
    Reference:      ${params.reference}
Quality Filter:     Fastp
    Prefix:         ${params.prefix.qc}
    Q:              ${params.fastp.Q}
    Trimming:
        Front:      ${params.fastp.fTrim}
        Tail:       ${params.fastp.tTrim}
Corrector:          Lordec
    k-mer:          ${params.lordec.k}
    s:              ${params.lordec.s}
Assembler:          ${params.assembler}
    Prefix:         ${params.prefix.assembly}
    Genome Size:    ${params.genome.size}${params.genome.unit}
Outdir:             ${params.outdir}
Threads:            ${params.threads}
__________________________

"""


workflow {
    main:
        nameCollect( short_reads_ch ) | set { short_reads } 
        nameCollect( long_reads_ch ) | set { long_reads }
    
        quality_filter( short_reads, long_reads, format_files )
        error_correction( quality_filter.out.ill, quality_filter.out.ont )
        assembly( quality_filter.out.ill, error_correction.out.preprocessing ).mix().set { assemblies }
        quality_filter.out.ont.mix( error_correction.out.preprocessing ).set {ont_reads}
        evaluation_reports( assemblies, ont_reads, reference_ch )

}
