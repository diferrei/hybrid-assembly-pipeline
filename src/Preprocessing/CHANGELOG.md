# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1] - 2020-23-09

### Added

- A local module that containts all functions to parallelize.
- Arguments parse for the algorithm.

## [1.0] - 2020-15-09

### New

- First model of preprocessing in julia.
