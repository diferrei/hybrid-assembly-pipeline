#!/usr/bin/env julia
using Base.Threads
using ArgParse, FASTX

path = @__DIR__
include(path * "/src/functions.jl")
using .Candidates, BioSequences


function parse_commandline()::Dict{String,Any}
    s = ArgParseSettings()
    s.description = "Pre-processing Algorithm of Oxford Nanopore sequences with Illumina sequences"
    s.commands_are_required = true
    s.version = "1.2"
    s.add_version = true
    @add_arg_table! s begin
        #! format: off
        "--ont"
            help = "Oxford Nanopore FASTA file input"
            required = true
        "--ill"
            help = "Illumina FASTA file input"
            required = true
        "--out"
            help = "Output FASTA file"
            default = "new_ont.fasta"
        "--t"
            help = "Change the mode: \n1-Illumina load at the beginning \n2-Illumina load by ONT"
            arg_type = Int
            default = 1

        #! format: on
    end
    

    return parse_args(s)
end


function charge_illumina(ont_seq::FASTA.Record, illumina_filename::String)::FASTA.Record
    ill_seq = [x for x in FASTA.Reader(open(illumina_filename, "r"))]
    return find_candidates(ont_seq, ill_seq)
end


function main()::Nothing
    args = parse_commandline()

    ont_seq = [x for x in FASTA.Reader(open(args["ont"], "r"))]
    new_ont = FASTA.Record[]

    if args["t"] == 1
        ill_seq = [x for x in FASTA.Reader(open(args["ill"], "r"))]
        @time @threads for q in ont_seq
            push!(new_ont, find_candidates(q, ill_seq))
        end
    elseif args["t"] == 2
        @time @threads for q in ont_seq
            push!(new_ont, charge_illumina(q, args["ill"]))
        end
    elseif args["t"] == 3
        # new_ont = pmap(charge_illumina,[q for q in ont_seq])
    else
        println("Don't exist this mode")
        return
    end
    #new_ont = pmap(seq -> find_candidates(seq, ill_seq), [x for x in ont_seq])

    open(args["out"], "w") do f
        ont_new_writer = FASTA.Writer(f)
        for i in new_ont
            write(ont_new_writer, i)
        end
    end

end


function reads_count(file_name::String)::Tuple{Integer,Integer}
    reads = 0
    open(FASTA.Reader, file_name) do f
	for x in f
	   reads += 1
	end
    end
    len = fldmod(reads,2)
    return (len[1]+len[2], len[1])
end

function main_test()::Nothing
    args = parse_commandline()

    new_ont = FASTA.Record[]
    ont_seq = FASTA.Record[]

    for (i,iter) in enumerate(reads_count(args["ont"]))
	ont_seq = [x for (idx,x) in enumerate(FASTA.Reader(open(args["ont"], "r"))) if idx >= (iter+1)*(i-1) && idx <= iter*i]
    	for (j, itr) in enumerate(reads_count(args["ill"]))
	    if args["t"] == 1
	        ill_seq = [x for (idx,x) in enumerate(FASTA.Reader(open(args["ill"], "r"))) if idx >= (itr+1)*(j-1) && idx <= itr*j]
            	@time  @threads for q in ont_seq
            	   push!(new_ont, find_candidates(q, ill_seq))
           	 end
    	    elseif args["t"] == 2
            	@time @threads for q in ont_seq
            	   push!(new_ont, charge_illumina(q, args["ill"]))
            	end
    	    elseif args["t"] == 3
        	# new_ont = pmap(charge_illumina,[q for q in ont_seq])
    	    else
            	println("Don't exist this mode")
            	return
    	    end
	end
    	#new_ont = pmap(seq -> find_candidates(seq, ill_seq), [x for x in ont_seq])
    end

    open(args["out"], "w") do f
        ont_new_writer = FASTA.Writer(f)
        for i in new_ont
            write(ont_new_writer, i)
        end
    end
end

main()
