# Pre processing

Code of Jacqueline Aldridge of pre processing that improves ONT sequences with Illumina sequences for de novo assembly.

## Requeriments

- [Julia](https://julialang.org) (>= 1.5.1)
    - [StringDistances](https://github.com/JuliaPackageMirrors/StringDistances.jl) (>=0.8)
    - [BioSequences](https://biojulia.net/BioSequences.jl/stable/) (>= 1.1)
    - [ArgParse](https://carlobaldassi.github.io/ArgParse.jl/stable/) (>= 1.1)

## Use

- Julia
  
To activate the environment use the following command to install the dependencies.
> `julia --project=@.`

And to use the program:
> `julia [--threads n] preprocess.jl --ont ont.fasta --ill illumina.fasta --out output.fasta`
