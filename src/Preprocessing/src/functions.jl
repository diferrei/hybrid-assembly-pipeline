module Candidates

using BioSequences, FASTX, StringDistances

include("./dna_counter.jl")

struct IlluminaCandidate
    index::Int64
    distance::Int64
end

struct OntCandidateInfo
    candidates::Vector{IlluminaCandidate}
    positions::Vector{Union{Int64,Missing}}
    OntCandidateInfo(size) = new([], fill(missing, size))
end


"""
    find_candidates(ont_seq, illumina_seqs)

Find the candidates to optimizer the ONT sequences.

# Arguments
- `ont::FASTA.Record`: Nanopore sequence.
- `ill::Vector{FASTA.Record}`: Illumina sequences.

# Details
The function return a improved Nanopore sequence in same input format
"""
function find_candidates(
    ont_seq::FASTA.Record,
    illumina_seqs::Vector{FASTA.Record},
)::FASTA.Record
    candidates_info = OntCandidateInfo(length(illumina_seqs))
    illumina_sizes = zeros(Int, length(illumina_seqs))
    seq_ont = FASTA.sequence(String, ont_seq)
    kmer_sizes = [23,25]
    for kmer_size in kmer_sizes
        oseq_index = create_ont_index(seq_ont, kmer_size)
        for (ill_idx, iseq) in enumerate(illumina_seqs)
            seq_ill = FASTA.sequence(String, iseq)
            illumina_sizes[ill_idx] = length(seq_ill)
            for (ill_slice_idx, slice) in enumerate(get_slices(seq_ill, kmer_size))
                if length(slice) > 20 &&
                   haskey(oseq_index, slice) &&
                   ismissing(candidates_info.positions[ill_idx])
                    evaluate_candidate!(
                        candidates_info,
                        seq_ont,
                        seq_ill,
                        oseq_index[slice],
                        kmer_size,
                        ill_slice_idx - 1,
                        ill_idx,
                    )
                end
            end
        end
        if length(candidates_info.candidates) > 0
            filter_candidates!(candidates_info, illumina_sizes)
        end
    end

    new_ont = ont_consensus(candidates_info, FASTA.sequence(ont_seq), illumina_seqs)

    return FASTA.Record(FASTA.identifier(ont_seq), FASTA.description(ont_seq), new_ont)
end


"""
    create_ont_index(seq, kmer_size)

Create a dictionary with all k-mers availables in a determined sequence.

# Arguments
- `seq::FASTA.Record`: Nanopore sequence.
- `kmer_size::Integer`: Size of k-mer.

# Details
The function return a dictionary with keys {subseq => position}.
"""
function create_ont_index(seq::String, kmer_size::Integer)::Dict{String,Integer}
    d = Dict{String,Integer}()
    for i = 1:(length(seq)-kmer_size+1)
        get!(d, seq[i:i+kmer_size-1], i)
    end
    return d
end


"""
    get_slices(seq, size)

Fragments a read (string) in slices of the same length (+ tail)

# Arguments
- `seq::String`: Sequence to split.
- `size::Integer`: Size of fragment.
"""
function get_slices(seq::String, size::Integer)::Vector{String}
    return [seq[i:min(i + size - 1, end)] for i = 1:size:length(seq)]
end


"""
    evaluate_candidate!(candidates_info, ont_sequence, illumina_sequence, pos, kmer_size, ill_slice_idx, ill_idx)

Cut out the Nanopore considering where the Illumina fits.

# Arguments
- `candidate_info::OntCandidateInfo`: Information of all the candidates
- `ont_sequence::String`: Nanopore sequence.
- `illumina_sequence::String`: Illumina sequence.
- `pos::Integer`: Position of Illumina subsequence.
- `kmer_size::Integer`: Size of k-mer.
- `ill_slice_idx::Integer`: Index of Illumina subsequence.
- `ill_idx::Integer`: Index of Illumina sequence.
"""
function evaluate_candidate!(
    candidates_info::OntCandidateInfo,
    ont_sequence::String,
    illumina_sequence::String,
    pos::Integer,
    kmer_size::Integer,
    ill_slice_idx::Integer,
    ill_idx::Integer,
)
    first = pos - kmer_size * ill_slice_idx
    last = first + length(illumina_sequence) - 1
    ont_fragment = ont_sequence[max(first, 1):min(last, length(ont_sequence))]

    dist = Levenshtein()(ont_fragment, illumina_sequence)

    if ill_idx in eachindex(candidates_info.candidates)
        if candidates_info.candidates[ill_idx].distance > dist
            candidates_info.positions[ill_idx] = first
        end
    else
        candidates_info.positions[ill_idx] = first
    end

    push!(candidates_info.candidates, IlluminaCandidate(ill_idx, dist))
end


"""
    filter_candidates!(candidates_info, illumina_sizes; max_dist_ratio)

Generate candidates of improved the ont and his position in the sequence.

# Arguments
- `candidates_info::OntCandidateInfo`: Dictionary of Illumina candidates.
- `illumina_sizes::Vector{Integer}`: Sizes of Illumina sequences.
- `max_dist_ratio::Float64`: Max distance allowed on a read to pass filter
"""
function filter_candidates!(
    candidates_info::OntCandidateInfo,
    illumina_sizes::Vector{Int};
    max_dist_ratio::Float64 = 0.25,
)
    # make a copy of current positions
    positions = copy(candidates_info.positions)
    # reset positions in object
    for i in eachindex(candidates_info.positions)
        candidates_info.positions[i] = missing
    end

    for (i, c) in Iterators.reverse(enumerate(candidates_info.candidates))
        max_dist = illumina_sizes[c.index] * max_dist_ratio
        if c.distance < max_dist
            candidates_info.positions[c.index] = positions[c.index]
        else
            deleteat!(candidates_info.candidates, i)
        end
    end
end

"""
    ont_consensus(candidates_info, ont_sequence, illumina_sequences)

Generate candidates of improved the ont and his position in the sequence.

# Arguments
- `candidates_info::OntCandidateInfo`: Dictionary of Illumina candidates.
- `ont_sequence::LongDNASeq`: Nanopore sequence.
- `illumina_sequences::Vector{FASTA.Record}`: Illumina sequences.
"""
function ont_consensus(
    candidates_info::OntCandidateInfo,
    ont_sequence::LongDNASeq,
    illumina_sequences::Vector{FASTA.Record},
)::LongDNASeq
    counters = [DNACounter() for _ in eachindex(ont_sequence)]

    for c in candidates_info.candidates
    	if candidates_info.positions[c.index] !== missing
          start = max(candidates_info.positions[c.index], 1)
          sequence = FASTA.sequence(illumina_sequences[c.index])

          if candidates_info.positions[c.index] < 1
              sequence = sequence[start-candidates_info.positions[c.index]+1:end]
          elseif length(sequence) > (length(ont_sequence) - start + 1)
              sequence = sequence[1:length(ont_sequence)-start+1]
          end

          @simd for i in eachindex(sequence)
              add!(counters[start+i-1], sequence[i], 100 / c.distance)
          end
	end
    end
    improved_ont = fill(DNA_N, length(ont_sequence))
    @simd for i in eachindex(counters)
        if counters[i].sum == 0
            # no coverage, put the original nucleotide
            @inbounds improved_ont[i] = ont_sequence[i]
        else
            # replace by maximum 
            @inbounds improved_ont[i] = counters[i].dna_bases[argmax(counters[i].counters)]
        end
    end

    return LongDNASeq(improved_ont)
end

export find_candidates
end
